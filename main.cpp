#include <iostream>
#include "Reader.h"
#include "Writer.h"

int main(int argc, char* argv[])
{
	try
	{
		Reader reader(argv[1]);
		Writer writer(reader, argv[2]);
	}   
	catch (std::exception const & er) {
		std::cerr << er.what() << std::endl;
		system("pause");
	}
	return 0;
}