#pragma once

#include "CheckErrorFile.h"
#include <string>
#include <list>
#include <fstream>
#include <map>
#include <cstddef>

struct Reader
{
public:
	using ListStr = std::list<std::string>;
	using MapStrInt = std::map<std::string, int>;

	Reader(const char* input_name);

	~Reader() = default;

	int get_sum_of_word() const;

	MapStrInt get_table() const;

private:
	ListStr node_;
	MapStrInt table_;
	int sum_of_word_;

	bool is_delim(const unsigned char & symb);

	void read_file(std::fstream & input);

	void search_str();

};
