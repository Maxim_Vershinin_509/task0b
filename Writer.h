#pragma once

#include "CheckErrorFile.h"
#include "Reader.h"
#include <vector>
#include <algorithm>


struct Writer
{
public:
	using MapStrInt = std::map<std::string, int>;
	using PairIntStr = std::pair<int, std::string>;
	using VectorPair = std::vector<PairIntStr>;

	Writer(const Reader & reader, const char* output_name);

	~Writer() = default;

private:
	Reader reader_;
	VectorPair res_table;

	void freq_sort();

	void print_word_count(std::fstream & output);
};