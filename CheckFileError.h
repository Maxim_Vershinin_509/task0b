#pragma once

#include <exception>
#include <fstream>

struct InputFileOpenError : std::exception
{
	InputFileOpenError(const std::fstream & file)
	{ }

	const char* what() const noexcept
	{
		return "An error occurred while opening the input file";
	}

};

struct BadInputName : std::exception
{
	BadInputName(const char* name)
	{ }

	const char* what() const noexcept
	{
		return "Please enter a valid input name";
	}

};

struct OutputFileOpenError : std::exception
{
	OutputFileOpenError(const std::fstream & file)
	{ }

	const char* what() const noexcept
	{
		return "An error occurred while opening the output file";
	}

};;
