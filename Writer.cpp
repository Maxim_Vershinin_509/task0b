#include "Writer.h"

Writer::Writer(const Reader & reader, const char* output_name) : reader_(reader)
{
	std::fstream output;
	output.open(output_name, std::fstream::out);
	if (output.fail())
	{
		throw OutputFileOpenError(output);
	}
	freq_sort();
	print_word_count(output);
}

void Writer::freq_sort()
{
	MapStrInt table = reader_.get_table();
	for (auto it = table.begin(); it != table.end(); it++)
	{
		res_table.push_back(make_pair(it->second, it->first));
	}
	auto great_pair = [](const PairIntStr & a, const PairIntStr & b)
	{
		return (a.first > b.first);
	};
	std::sort(res_table.begin(), res_table.end(), great_pair);

}

void Writer::print_word_count(std::fstream & output)
{
	for (auto it = res_table.begin(); it != res_table.end(); it++)
	{
		double freq_perc = ((double)it->first / reader_.get_sum_of_word()) * 100;
		output << it->second << ";" << it->first << ";";
		output << freq_perc << "%" << std::endl;
	}
}