#include "Reader.h"

Reader::Reader(const char* input_name) : sum_of_word_(0)
{
	if (input_name == nullptr) {
		throw BadInputName(input_name);
	}
	std::fstream input;
	input.open(input_name, std::fstream::in);
	if (input.fail())
	{
		throw InputFileOpenError(input);
	}
	read_file(input);
	input.close();
	search_str();
}

int Reader::get_sum_of_word() const
{
	return sum_of_word_;
}

Reader::MapStrInt Reader::get_table() const
{
	return table_;
}

bool Reader::is_delim(const unsigned char & symb)
{
	if ((symb >= 0 && symb < '0') || (symb > '9' && symb < 'A') || (symb > 'Z' && symb < 'a') || symb > 'z') {
		return true;
	}
	return false;
}

void Reader::read_file(std::fstream & input)
{
	std::string str;
	while (std::getline(input, str, ' '))
	{
		if (str != "")
			node_.push_back(str);
	}
}

void Reader::search_str()
{
	for (auto it = node_.begin(); it != node_.end(); it++)
	{
		size_t first_ind_str = 0;
		for (size_t i = 0; i != it->length() + 1; i++)
		{
			if (is_delim(static_cast<unsigned char>((*it)[i])) || i == it->length())
			{
				if (std::string(*it, first_ind_str, i - first_ind_str) != "")
				{
					table_[std::string(*it, first_ind_str, i - first_ind_str)]++;
					sum_of_word_++;
				}
				first_ind_str = i + 1;
			}
		}
	}
}